﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ChaxAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BatchController : ControllerBase
    {
        private readonly IBatchService _batchService;

        public BatchController(IBatchService batchService)
        {
            _batchService = batchService;
        }


        // GET: api/<BatchController>
        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            return Ok(await _batchService.GetAllAsync());
        }

        // GET api/<BatchController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult>Get(int id)
        {
            return Ok(await _batchService.GetId(id));
        }

        // POST api/<BatchController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] BATCH batch)
        {
            return Ok(await _batchService.CreateAsync(batch));
        }

        // PUT api/<BatchController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] BATCH batch)
        {
            return Ok(await _batchService.UpdateAsync(batch));
        }

        // DELETE api/<BatchController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await _batchService.DeleteAsync(id));
        }
    }
}
