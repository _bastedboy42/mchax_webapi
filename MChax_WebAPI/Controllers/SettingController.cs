﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ChaxAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SettingController : ControllerBase
    {
        private readonly ISettingService _setting;

        public SettingController(ISettingService setting)
        {
            _setting = setting;
        }

        // GET: api/<SettingController>
        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            return Ok(await _setting.GetAllAsync());
        }

        // GET api/<SettingController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await _setting.GetId(id));
        }

        // POST api/<SettingController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] SETTING setting)
        {
            return Ok(await _setting.CreateAsync(setting));
        }

        // PUT api/<SettingController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] SETTING setting)
        {
            return Ok(await _setting.UpdateAsync(setting));
        }

        // DELETE api/<SettingController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await _setting.DeleteAsync(id));
        }
    }
}
