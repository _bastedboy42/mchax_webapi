﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ChaxAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserPermissionController : ControllerBase
    {
        private readonly IUserPermissionService _userPermission;

        public UserPermissionController(IUserPermissionService userPermission)
        {
            _userPermission = userPermission;
        }

        // GET: api/<UserPermissionController>
        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            return Ok(await _userPermission.GetAllAsync());
        }

        // GET api/<UserPermissionController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await _userPermission.GetId(id));
        }

        // POST api/<UserPermissionController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] USERPERMISSIONS userPermission)
        {
            return Ok(await _userPermission.CreateAsync(userPermission));
        }

        // PUT api/<UserPermissionController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] USERPERMISSIONS userPermission)
        {
            return Ok(await _userPermission.CreateAsync(userPermission));
        }

        // DELETE api/<UserPermissionController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await _userPermission.DeleteAsync(id));
        }
    }
}
