﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ChaxAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PayeeController : ControllerBase
    {
        private readonly IPayeeService _payeeService;

        public PayeeController(IPayeeService payeeService)
        {
            _payeeService = payeeService;
        }

        // GET: api/<PayeeController>
        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            return Ok(await _payeeService.GetAllAsync());
        }

        // GET api/<PayeeController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await _payeeService.GetId(id));
        }

        // POST api/<PayeeController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] PAYEE payee)
        {
            return Ok(await _payeeService.CreateAsync(payee));
        }

        // PUT api/<PayeeController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] PAYEE payee)
        {
            return Ok(await _payeeService.UpdateAsync(payee));
        }

        // DELETE api/<PayeeController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await _payeeService.DeleteAsync(id));
        }
    }
}
