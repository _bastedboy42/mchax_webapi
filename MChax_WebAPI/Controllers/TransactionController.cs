﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ChaxAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private readonly ITransactionService _transactionService;

        public TransactionController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }


        // GET: api/<TransactionController>
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _transactionService.GetAllAsync());
        }

        // GET api/<TransactionController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await _transactionService.GetId(id));
        }

        // POST api/<TransactionController>
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] TRANSACTION transaction)
        {
            return Ok(await _transactionService.CreateAsync(transaction));
        }

        // PUT api/<TransactionController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, TRANSACTION transaction)
        {
            return Ok(await _transactionService.UpdateAsync(transaction));
        }

        // DELETE api/<TransactionController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await _transactionService.DeleteAsync(id));
        }
    }
}
