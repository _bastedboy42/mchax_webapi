﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ChaxAPI.Component;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.Data;
using ChaxAPI.DTO;
using ChaxAPI.Extension;
using ChaxAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace ChaxAPI.Controllers
{
//    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UserController:ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;

        public UserController(IUserService userService, IMapper mapper, IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }


        // [AllowAnonymous]
        // [HttpPost("authenticate")]
        // public IActionResult Authenticate([FromBody]UserAuthenticateDto model)
        // {
        //     var user = _userService.Authenticate(model.EMAIL, model.PASSWORD);
        //
        //     if (user == null)
        //         return BadRequest(new { message = "Username or password is incorrect" });
        //
        //     var tokenHandler = new JwtSecurityTokenHandler();
        //     var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
        //     var tokenDescriptor = new SecurityTokenDescriptor
        //     {
        //         Subject = new ClaimsIdentity(new Claim[]
        //         {
        //             new Claim(ClaimTypes.Name, user.USERID.ToString())
        //         }),
        //         Expires = DateTime.UtcNow.AddDays(7),
        //         SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        //     };
        //     var token = tokenHandler.CreateToken(tokenDescriptor);
        //     var tokenString = tokenHandler.WriteToken(token);
        //
        //     // return basic user info and authentication token
        //     return Ok(new
        //     {
        //         Id = user.USERID,
        //         Username = user.USERNAME,
        //         FirstName = user.FIRSTNAME,
        //         LastName = user.LASTNAME,
        //         Token = tokenString
        //     });
        // }
        //
        // [AllowAnonymous]
        // [HttpPost("register")]
        // public IActionResult Register([FromBody]UserRegisterDto model)
        // {
        //     // map model to entity
        //     var user = _mapper.Map<USER>(model);
        //
        //     try
        //     {
        //         // create user
        //         _userService.Create(user, model.PASSWORD);
        //         return Ok();
        //     }
        //     catch (ChaxException ex)
        //     {
        //         // return error message if there was an exception
        //         return BadRequest(new { message = ex.Message });
        //     }
        // }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            var users = await _userService.GetAllAsync();
            var model = _mapper.Map<IList<UserDto>>(users);
            return Ok(model);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var user = _userService.GetById(id);
            var model = _mapper.Map<UserDto>(user);
            return Ok(model);
        }
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]UserUpdateDto model)
        {
            // map model to entity and set id
            var user = _mapper.Map<USER>(model);
            user.USERID = id;

            try
            {
                // update user 
                _userService.Update(user, model.PASSWORD);
                return Ok();
            }
            catch (ChaxException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _userService.Delete(id);
            return Ok();
        }

    }
}