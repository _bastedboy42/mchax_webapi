﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ChaxAPI.Component;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.DTO;
using ChaxAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ChaxAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    
    public class BankController : ControllerBase
    {
        private readonly IBankService _bankService;
        private readonly IMapper _mapper;

        public BankController(IBankService bankService, IMapper mapper)
        {
            _bankService = bankService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var model = await _bankService.GetAllAsync();
            return Ok(model);
        }

        [HttpPost("create")]
        public async Task<IActionResult> InsertNewBank([FromBody] BANK bank)
        {
           await _bankService.CreateAsync(bank);
           return Ok();
        }

        [HttpGet("{routingNUmber}")]
        public async Task<IActionResult> GetBankByRoutingNumber(string routingNUmber)
        {
            var bank = await _bankService.GetBankByRoutingAsync(routingNUmber);
            return Ok(bank);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] BANK bank)
        {
            await _bankService.UpdateAsync(bank);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _bankService.DeleteAsync(id);
            return Ok();
        }
    }
}
