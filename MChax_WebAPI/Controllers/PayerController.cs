﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ChaxAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PayerController : ControllerBase
    {
        private readonly IPayerService _payerService;

        public PayerController(IPayerService payerService)
        {
            _payerService = payerService;
        }

        // GET: api/<PayerController>
        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            return Ok(await _payerService.GetAllAsync());
        }

        // GET api/<PayerController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await _payerService.GetId(id));
        }

        // POST api/<PayerController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] PAYER payer)
        {
            return Ok(await _payerService.CreateAsync(payer));
        }

        // PUT api/<PayerController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] PAYER payer)
        {
            return Ok(await _payerService.UpdateAsync(payer));
        }

        // DELETE api/<PayerController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await _payerService.DeleteAsync(id));
        }
    }
}
