﻿using AutoMapper;
using ChaxAPI.DTO;
using ChaxAPI.Models;

namespace ChaxAPI
{
    public class AutoMapperProfile: Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<USER, UserDto>();
            CreateMap<UserRegisterDto, USER>();
            CreateMap<UserUpdateDto, USER>();
        }

    }
}