﻿using Dapper.Contrib.Extensions;

namespace ChaxAPI.Models
{

    [Table("USERS")]
    public class USER
    {
        /// <summary>
        /// The username
        /// </summary>
        [Key]
        public int USERID { get; set; }
        /// <summary>
        /// The username of the user
        /// </summary>
        public string USERNAME { get; set; }

        /// <summary>
        /// User's first name
        /// </summary>
        public string FIRSTNAME { get; set; }

        /// <summary>
        /// User's last name
        /// </summary>
        public string LASTNAME { get; set; }

        /// <summary>
        /// The email address use when signing up
        /// </summary>
        public string EMAIL { get; set; }
        /// <summary>
        /// The password salt
        /// </summary>
        public byte[] PASSWORDSALT { get; set; }
        /// <summary>
        /// The hash
        /// </summary>
        public byte[] PASSWORDHASH { get; set; }

        /// <summary>
        /// Default payee for the user
        /// </summary>
        public string DEFAULTPAYEE { get; set; }
    }
}
