﻿using System;
using Dapper.Contrib.Extensions;

namespace ChaxAPI.Models
{
    [Table("BATCH")]
    public class BATCH
    {
        [Key]
        public int ID { get; set; }

        public string PAYEENAME { get; set; }

        public string PAYEEADDRESS1 { get; set; }

        public string PAYEEADDRESS2 { get; set; }

        public string PAYEEADDRESS3 { get; set; }

        public string PAYEEADDRESS4 { get; set; }

        public string PAYEEPHONE { get; set; }

        public string PAYEEMICR { get; set; }

        public string PAYERNAME { get; set; }

        public string PAYERPHONE { get; set; }

        public string PAYERADDRESS1 { get; set; }

        public string PAYERADDRESS2 { get; set; }

        public string PAYERADDRESS3 { get; set; }

        public string PAYERADDRESS4 { get; set; }

        public string TRANSIT { get; set; }

        public DateTime? CHECKDATE { get; set; }

        public decimal? AMOUNT { get; set; }

        public string BANKNAME { get; set; }

        public string BANKADDRESS1 { get; set; }

        public string BANKADDRESS2 { get; set; }

        public string MEMO { get; set; }

        public string CHECKNUMBER { get; set; }

        public string USERID { get; set; }

        public string SERIALNO { get; set; }

        public string ACCOUNT { get; set; }

        public string ROUTING { get; set; }

    }
}