﻿using System.ComponentModel.DataAnnotations.Schema;
using DevExpress.Xpo;

namespace ChaxAPI.Models
{
    [Table("PAYERS")]
    public class PAYER
    {
        [Key]
        public int ID { get; set; }

        public string NAME { get; set; }

        public byte[] PHONE { get; set; }

        public string ACCOUNT { get; set; }

        public string ADDRESS1 { get; set; }

        public string ADDRESS2 { get; set; }

        public string ADDRESS3 { get; set; }

        public string ADDRESS4 { get; set; }

        public int? BANKID { get; set; }
    }
}