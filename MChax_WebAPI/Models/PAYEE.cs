﻿

using Dapper.Contrib.Extensions;

namespace ChaxAPI.Models
{
    [Table("PAYEES")]
    public class PAYEE
    {
        [Key]
        public int ID { get; set; }

        public string NAME { get; set; }

        public string ADDRESS1 { get; set; }

        public string ADDRESS2 { get; set; }

        public string ADDRESS3 { get; set; }

        public string ADDRESS4 { get; set; }

        public string PHONE { get; set; }

        public bool? ISDEFAULT { get; set; }
    }
}