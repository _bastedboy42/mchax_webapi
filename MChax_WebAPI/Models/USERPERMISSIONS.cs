﻿using Dapper.Contrib.Extensions;

namespace ChaxAPI.Models
{
    [Table("USERPERMISSIONS")]
    public class USERPERMISSIONS
    {
        public int ID { get; set; }

        public bool? ISOKMEMORIZE { get; set; }

        public string SIGNATUREMESSAGE { get; set; }

        public bool? ISCHAXPERMISSION { get; set; }

        public string USERID { get; set; }

        public bool? CANPAYEEENTER { get; set; }

        public bool? CANPAYEESELECT { get; set; }

        public bool? CANTRANSACTIONREPORT { get; set; }

        public bool? CANBATCHPRINT { get; set; }

        public bool? CANTRANSACTIONDELETE { get; set; }

        public bool? ISADMIN { get; set; }
    }
}