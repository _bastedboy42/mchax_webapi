﻿using System;
using Dapper.Contrib.Extensions;

namespace ChaxAPI.Models
{

    [Table("BANKS")]
    public class BANK
    {
        [Key]
        public int ID { get; set; }

        public string BANKNAME { get; set; }

        public string BRANCH { get; set; }

        public string ADDRESS { get; set; }

        public string CITYSTATEZIP { get; set; }

        public string ROUTING { get; set; }

        public string TRANSITSYMBOL { get; set; }

        public DateTime? FILEDATE { get; set; }

        public string TYPE { get; set; }

        public string BRANCHCODE { get; set; }
    }
}