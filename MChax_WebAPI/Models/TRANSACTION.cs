﻿using System;
using Dapper.Contrib.Extensions;


namespace ChaxAPI.Models
{
    [Table("TRANSACTIONS")]
    public class TRANSACTION
    {
        [Key]
        public int ID { get; set; }

        public string PAYEENAME { get; set; }

        public string PAYEEAD1 { get; set; }

        public string PAYEEAD2 { get; set; }

        public string PAYEEAD3 { get; set; }

        public string PAYEEAD4 { get; set; }

        public string PAYEEPHONE { get; set; }

        public string ACCOUNT { get; set; }

        public string PAYER { get; set; }

        public string PHONE { get; set; }

        public string PAYERAD1 { get; set; }

        public string PAYERAD2 { get; set; }

        public string PAYERAD3 { get; set; }

        public string PAYERAD4 { get; set; }

        public string TRANSIT { get; set; }

        public DateTime? CHECKDATE { get; set; }

        public decimal? AMOUNT { get; set; }

        public string BANKNAME { get; set; }

        public string BANKADDRESS1 { get; set; }

        public string BANKADDRESS2 { get; set; }

        public string MEMO { get; set; }

        public string CHECKNUMBER { get; set; }

        public string USERID { get; set; }

        public DateTime? DATEPRINTED { get; set; }

        public string SEARCH { get; set; }

        public string FREQDAY { get; set; }
    }
}