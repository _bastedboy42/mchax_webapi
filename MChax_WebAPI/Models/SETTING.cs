﻿using Dapper.Contrib.Extensions;


namespace ChaxAPI.Models
{
    [Table("SETTINGS")]
    public class SETTING
    {
        [Key]
        public int? ID { get; set; }

        public string USERNAME { get; set; }

        public string SETTINGNAME { get; set; }

        public string SETTINGVALUE { get; set; }

    }
}