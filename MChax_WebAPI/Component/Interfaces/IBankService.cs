﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ChaxAPI.Models;

namespace ChaxAPI.Component.Interfaces
{
    public interface IBankService
    {
        // sync
        IEnumerable<BANK> GetAll();
        BANK GetBankByRouting(string routingNumber);
        int Create(BANK bank);
        bool Update(BANK bank);
        bool Delete(int id);


        // async
        Task<IEnumerable<BANK>> GetAllAsync();
        Task<BANK> GetId(int id);
        Task<BANK> GetBankByRoutingAsync(string routingNumber);
        Task<int> CreateAsync(BANK bank);
        Task<bool> UpdateAsync(BANK bank);
        Task<bool> DeleteAsync(int id);
    }
}