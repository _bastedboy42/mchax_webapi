﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ChaxAPI.Models;

namespace ChaxAPI.Component.Interfaces
{
    public interface IUserPermissionService
    {
        Task<List<USERPERMISSIONS>> GetAllAsync();
        Task<USERPERMISSIONS> GetId(int id);
        Task<int> CreateAsync(USERPERMISSIONS user);
        Task<bool> UpdateAsync(USERPERMISSIONS user);
        Task<bool> DeleteAsync(int id);
    }
}