﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ChaxAPI.Models;

namespace ChaxAPI.Component.Interfaces
{
    public interface ITransactionService
    {
        Task<List<TRANSACTION>> GetAllAsync();
        Task<TRANSACTION> GetId(int id);
        Task<int> CreateAsync(TRANSACTION transaction);
        Task<bool> UpdateAsync(TRANSACTION transaction);
        Task<bool> DeleteAsync(int id);
    }
}