﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ChaxAPI.Models;

namespace ChaxAPI.Component.Interfaces
{
    public interface IUserService
    {
        USER Authenticate(string username, string password);
        IEnumerable<USER> GetAll();
        Task<IEnumerable<USER>> GetAllAsync();
        USER GetById(int id);
        USER GetByUserName(string username);
        USER GetByEmailAddress(string emailAddress);
        USER Create(USER user, string password);
        void Create(USER user);
        void Update(USER user);
        void Update(USER user, string password=null);
        void Delete(int id);
    }
}