﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ChaxAPI.Models;

namespace ChaxAPI.Component.Interfaces
{
    public interface IBatchService
    {
        // async
        Task<List<BATCH>> GetAllAsync();
        Task<BATCH> GetId(int id);
        Task<int> CreateAsync(BATCH batch);
        Task<bool> UpdateAsync(BATCH batch);
        Task<bool> DeleteAsync(int id);
    }
}