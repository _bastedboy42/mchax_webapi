﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ChaxAPI.Models;

namespace ChaxAPI.Component.Interfaces
{
    public interface IPayeeService
    {
        // async
        Task<List<PAYEE>> GetAllAsync();
        Task<PAYEE> GetId(int id);
        Task<int> CreateAsync(PAYEE payee);
        Task<bool> UpdateAsync(PAYEE payee);
        Task<bool> DeleteAsync(int id);
    }
}