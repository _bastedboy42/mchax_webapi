﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ChaxAPI.Models;

namespace ChaxAPI.Component.Interfaces
{
    public interface ISettingService
    {
        Task<List<SETTING>> GetAllAsync();
        Task<SETTING> GetId(int id);
        Task<int> CreateAsync(SETTING setting);
        Task<bool> UpdateAsync(SETTING setting);
        Task<bool> DeleteAsync(int id);
    }
}