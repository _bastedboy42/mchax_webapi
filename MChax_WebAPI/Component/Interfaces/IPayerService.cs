﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ChaxAPI.Models;

namespace ChaxAPI.Component.Interfaces
{
    public interface IPayerService
    {
        Task<List<PAYER>> GetAllAsync();
        Task<PAYER> GetId(int id);
        Task<int> CreateAsync(PAYER payer);
        Task<bool> UpdateAsync(PAYER payer);
        Task<bool> DeleteAsync(int id);
    }
}