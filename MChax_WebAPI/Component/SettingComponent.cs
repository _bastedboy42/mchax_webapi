﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.Models;

namespace ChaxAPI.Component
{
    public class SettingComponent
    {
        private readonly ISettingService _settingService;

        public SettingComponent(ISettingService settingService)
        {
            _settingService = settingService;
        }

        public async Task<List<SETTING>> GetAllAsync()
        {
            return await _settingService.GetAllAsync();
        }

        public async Task<int> CreateAsync(SETTING setting)
        {
            return await _settingService.CreateAsync(setting);
        }

        public async Task<bool> UpdateAsync(SETTING setting)
        {
            return await _settingService.UpdateAsync(setting);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            return await _settingService.DeleteAsync(id);
        }
    }
}