﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.Models;

namespace ChaxAPI.Component
{
    public class PayerComponent
    {
        private readonly IPayerService _payerService;

        public PayerComponent(IPayerService payerService)
        {
            _payerService = payerService;
        }

        public async Task<List<PAYER>> GetAllAsync()
        {
            return (await _payerService.GetAllAsync()).ToList();
        }

        public async Task<int> CreateAsync(PAYER payer)
        {
            return await _payerService.CreateAsync(payer);
        }

        public async Task<bool> UpdateAsync(PAYER payer)
        {
            return await _payerService.UpdateAsync(payer);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            return await _payerService.DeleteAsync(id);
        }
    }
}