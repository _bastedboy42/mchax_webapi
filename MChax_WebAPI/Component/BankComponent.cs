﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.Models;

namespace ChaxAPI.Component
{
    public class BankComponent
    {
        private readonly IBankService _bankService;

        public BankComponent(IBankService bankService)
        {
            _bankService = bankService;
        }


        public IEnumerable<BANK> GetAll()
        {
            return _bankService.GetAll();
        }

        public BANK GetBankByRouting(string routingNumber)
        {
            return _bankService.GetBankByRouting(routingNumber);
        }

        public int Create(BANK bank)
        {
            return _bankService.Create(bank);
        }

        public bool Update(BANK bank)
        {
            return _bankService.Update(bank);
        }

        public bool Delete(int id)
        {
            return _bankService.Delete(id);
        }

        public async  Task<IEnumerable<BANK>> GetAllAsync()
        {
            return await _bankService.GetAllAsync();
        }

        public async Task<BANK> GetId(int id)
        {
            return await _bankService.GetId(id);
        }

        public async Task<BANK> GetBankByRoutingAsync(string routingNumber)
        {
            return await _bankService.GetBankByRoutingAsync(routingNumber);
        }

        public async Task<int> CreateAsync(BANK bank)
        {
            return await _bankService.CreateAsync(bank);
        }

        public async Task<bool> UpdateAsync(BANK bank)
        {
            return await _bankService.UpdateAsync(bank);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            return await _bankService.DeleteAsync(id);
        }
    }
}