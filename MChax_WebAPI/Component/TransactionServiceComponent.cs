﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.Models;
using DevExpress.DataProcessing.InMemoryDataProcessor;

namespace ChaxAPI.Component
{
    public class TransactionServiceComponent 
    {
        private readonly ITransactionService _transactionService;

        public TransactionServiceComponent(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        public async Task<List<TRANSACTION>> GetAllAsync()
        {
            return await _transactionService.GetAllAsync();
        }
        public async Task<TRANSACTION> GetId(int id)
        {
            return await _transactionService.GetId(id);
        }

        public async Task<int> CreateAsync(TRANSACTION transaction)
        {
            return await _transactionService.CreateAsync(transaction);
        }

        public async Task<bool> UpdateAsync(TRANSACTION transaction)
        {
            return await _transactionService.UpdateAsync(transaction);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            return await _transactionService.DeleteAsync(id);
        }
    }
}