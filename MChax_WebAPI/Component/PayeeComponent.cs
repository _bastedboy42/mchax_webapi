﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.Models;

namespace ChaxAPI.Component
{
    public class PayeeComponent
    {
        private readonly IPayeeService _payeeService;

        public PayeeComponent(IPayeeService payeeService)
        {
            _payeeService = payeeService;
        }

        public  async Task<List<PAYEE>> GetAllAsync()
        {
            return await _payeeService.GetAllAsync();
        }

        public async Task<int> CreateAsync(PAYEE payee)
        {
            return await _payeeService.CreateAsync(payee);
        }

        public  async Task<bool> UpdateAsync(PAYEE payee)
        {
            return await _payeeService.UpdateAsync(payee);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            return await _payeeService.DeleteAsync(id);
        }
    }
}