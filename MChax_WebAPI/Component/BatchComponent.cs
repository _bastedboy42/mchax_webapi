﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.Models;

namespace ChaxAPI.Component
{
    public class BatchComponent 
    {
        private readonly IBatchService _batchService;

        public BatchComponent(IBatchService batchService)
        {
            _batchService = batchService;
        }

        public async Task<List<BATCH>> GetAllAsync()
        {
            return await _batchService.GetAllAsync();
        }

        public async Task<int> CreateAsync(BATCH batch)
        {
            return await _batchService.CreateAsync(batch);
        }

        public async Task<bool> UpdateAsync(BATCH banks)
        {
            return await _batchService.UpdateAsync(banks);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            return await _batchService.DeleteAsync(id);
        }
    }
    
}