﻿using System;
using System.Collections.Generic;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.Extension;
using ChaxAPI.Models;

namespace ChaxAPI.Component
{
    public class UserComponent  
    {
        private readonly IUserService _userService;

        public UserComponent(IUserService userService) => _userService = userService;

        public USER Authenticate(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return null;

            var user = _userService.GetByUserName(username);
            if (user == null)
                return null;
            // check if password is correct
            if (!VerifyPasswordHash(password, user.PASSWORDHASH, user.PASSWORDSALT))
                return null;

            return user;
        }
        public IEnumerable<USER> GetAll() => _userService.GetAll();

        public USER GetById(int id) => _userService.GetById(id);

        public void Create(USER user, string password)
        {
            if (string.IsNullOrWhiteSpace(password))
                throw new ChaxException("Password is required");

            if (_userService.GetByUserName(user.USERNAME)!=null)
            {
                 throw new ChaxException("Username \"" + user.USERNAME + "\" is already taken");
            }

               

            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(password, out passwordHash, out passwordSalt);

            _userService.Create(user);
        }

        public void Update(USER user, string password = null) => _userService.Update(user);

        public void Delete(int id) => _userService.Delete(id);



        // private helper methods

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }

    }
}
