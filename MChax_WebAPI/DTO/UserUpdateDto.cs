﻿namespace ChaxAPI.DTO
{
    public class UserUpdateDto
    {
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string USERNAME { get; set; }
        public string PASSWORD { get; set; }
    }
}