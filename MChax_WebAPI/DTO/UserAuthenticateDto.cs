﻿using System.ComponentModel.DataAnnotations;

namespace ChaxAPI.DTO
{
    public class UserAuthenticateDto
    {
        [Required]
        public string EMAIL { get; set; }

        [Required]
        public string PASSWORD { get; set; }

        // un : derstinebuyagan
        // pw: LPqv0vka42
    }
}