﻿namespace ChaxAPI.DTO
{
    public class UserDto
    {
        public int USERID { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string USERNAME { get; set; }


        public string DEFAULTPAYEE { get; set; }
        public string FULLNAME => $"{FIRSTNAME} {LASTNAME}";

    }
}