﻿using System.ComponentModel.DataAnnotations;

namespace ChaxAPI.DTO
{
    public class UserRegisterDto
    {
        [Required]
        public string FIRSTNAME { get; set; }

        [Required]
        public string LASTNAME { get; set; }

        [Required]
        public string USERNAME { get; set; }

        [Required]
        public string EMAIL { get; set; }

        [Required]
        public string PASSWORD { get; set; }
    }
}