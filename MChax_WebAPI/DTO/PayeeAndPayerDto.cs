﻿using System;
using System.Text;
using ChaxAPI.Extension;

namespace ChaxAPI.DTO
{
    public class PayeeAndPayerDto
    {

        #region PAYER

        /// <summary>
        /// PayerName
        /// </summary>
        public string PayerName { get; set; }
        /// <summary>
        /// First Payer Address
        /// </summary>
        public string PayerAd1 { get; set; }
        /// <summary>
        /// Second Payer Address
        /// </summary>
        public string PayerAd2 { get; set; }
        /// <summary>
        /// Third Payer Address
        /// </summary>
        public string PayerAd3 { get; set; }
        /// <summary>
        /// Fourth Payer Address
        /// </summary>
        public string PayerAd4 { get; set; }

        /// <summary>
        /// Payer Phone Number
        /// </summary>
        public string PayerAdPhone { get; set; }

        public string PayerNameAddress
        {
            get
            {
                string payer = string.Empty;
                if (!string.IsNullOrEmpty(PayerName))
                    if (PayerName.Trim().Length > 0) payer += PayerName;
                if (!string.IsNullOrEmpty(PayerAd1))
                    if (PayerAd1.Trim().Length > 0) payer = payer + "\n" + PayerAd1;
                if (!string.IsNullOrEmpty(PayerAd2))
                    if (PayerAd2.Trim().Length > 0) payer = payer + ", " + PayerAd2;
                if (!string.IsNullOrEmpty(PayerAd3))
                    if (PayerAd3.Trim().Length > 0) payer = payer + "\n" + PayerAd3;
                if (!string.IsNullOrEmpty(PayerAd4))
                    if (PayerAd4.Trim().Length > 0) payer = payer + ", " + PayerAd4;
                if (!string.IsNullOrEmpty(PayerAdPhone))
                    if (PayerAdPhone.Trim().Length > 0) payer = payer + "\n" + PayerAdPhone;
                return payer;
            }
        }

        public string PayerAddress
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                if (!string.IsNullOrEmpty(PayerAd1))
                    if (PayerAd1.Trim().Length > 0)
                        sb.AppendLine(PayerAd1);
                if (!string.IsNullOrEmpty(PayerAd2))
                    if (PayerAd2.Trim().Length > 0)
                        sb.AppendLine(PayerAd2);
                if (!string.IsNullOrEmpty(PayerAd3))
                    if (PayerAd3.Trim().Length > 0)
                        sb.AppendLine(PayerAd3);
                if (!string.IsNullOrEmpty(PayerAd4))
                    if (PayerAd4.Trim().Length > 0)
                        sb.AppendLine(PayerAd4);
                if (!string.IsNullOrEmpty(PayerAdPhone))
                    if (PayerAdPhone.Trim().Length > 0)
                        sb.AppendLine(PayerAdPhone);
                return sb.ToString();
            }
        }

        #endregion


        /// <summary>
        /// Payee Name or Pay to the Order Of
        /// </summary>
        public string Payee { get; set; }
        /// <summary>
        /// Payee First Address
        /// </summary>
        public string PayeeAd1 { get; set; }
        /// <summary>
        /// Payee Second Address
        /// </summary>
        public string PayeeAd2 { get; set; }
        /// <summary>
        /// Payee Third Address
        /// </summary>
        public string PayeeAd3 { get; set; }
        /// <summary>
        /// Payee Fourth Address
        /// </summary>
        public string PayeeAd4 { get; set; }

        /// <summary>
        /// Payee Phone
        /// </summary>
        public string PayeePhone { get; set; }



        public string PayeeNameAddress
        {
            get
            {
                string payer = string.Empty;
                if (!string.IsNullOrEmpty(Payee))
                    if (Payee.Trim().Length > 0) payer += Payee;
                if (!string.IsNullOrEmpty(PayeeAd1))
                    if (PayeeAd1.Trim().Length > 0) payer = payer + "\n" + PayeeAd1;
                if (!string.IsNullOrEmpty(PayeeAd2))
                    if (PayeeAd2.Trim().Length > 0) payer = payer + ", " + PayeeAd2;
                if (!string.IsNullOrEmpty(PayeeAd3))
                    if (PayeeAd3.Trim().Length > 0) payer = payer + "\n" + PayeeAd3;
                if (!string.IsNullOrEmpty(PayeeAd4))
                    if (PayeeAd4.Trim().Length > 0) payer = payer + ", " + PayeeAd4;
                if (!string.IsNullOrEmpty(PayeePhone))
                    if (PayeePhone.Trim().Length > 0) payer = payer + "\n" + PayeePhone;
                return payer;
            }
        }

        public string PayeeAddress
        {
            get
            {
                string payer = string.Empty;
                if (!string.IsNullOrEmpty(PayeeAd1))
                    if (PayeeAd1.Trim().Length > 0) payer = PayeeAd1;
                if (!string.IsNullOrEmpty(PayeeAd2))
                    if (PayeeAd2.Trim().Length > 0) payer = payer + "\n" + PayeeAd2;
                if (!string.IsNullOrEmpty(PayeeAd3))
                    if (PayeeAd3.Trim().Length > 0) payer = payer + "\n" + PayeeAd3;
                if (!string.IsNullOrEmpty(PayeeAd4))
                    if (PayeeAd4.Trim().Length > 0) payer = payer + "\n" + PayeeAd4;
                return payer;
            }
        }




        /// <summary>
        /// The account number + routing
        /// </summary>
        public string Account => $"{Routing} {AccountNumber}";

        public string RoutingCheckAccount => $"{Routing} {AccountNumber} {CheckNumber}";

        public string RoutingAccountCheckWithFormatting
        {
            get
            {
                if (IsEPC)
                {
                    return $"C{CheckNumber}C 6A{Routing}A {AccountNumber}";
                }
                return $"C{CheckNumber}C A{Routing}A {AccountNumber}";
            }
        }

        public bool IsEPC { get; set; }

        /// <summary>
        /// Routing Number
        /// </summary>
        public string Routing { get; set; }
        /// <summary>
        /// The account Number 
        /// </summary>
        public string AccountNumber { get; set; }
        /// <summary>
        /// Transit Number
        /// </summary>
        public string Transit { get; set; }

        /// <summary>
        /// The check amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Amount in words
        /// </summary>
        public string AmountInWords => Amount.ToWords();


        /// <summary>
        /// Bank Name 
        /// </summary>
        public string BankName { get; set; }
        /// <summary>
        /// Bank Address
        /// </summary>
        public string BankAddress { get; set; }
        /// <summary>
        /// Bank City State and Zip
        /// </summary>
        public string BankCityStateZip { get; set; }


        public string BankFullAddress
        {
            get { return BankAddress + "\n" + BankCityStateZip; }
        }

        public string BankFullAddressWithTransit => BankAddress + "\n" + BankCityStateZip + Transit;

        /// <summary>
        /// The memo written on the check
        /// </summary>
        public string Memo { get; set; }
        /// <summary>
        /// Check Number
        /// </summary>
        public string CheckNumber { get; set; }
        /// <summary>
        /// Created Date of the Check
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Signature message on check
        /// </summary>
        public string SignatureMessage { get; set; }

    }
}
