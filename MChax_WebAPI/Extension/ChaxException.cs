﻿using System;
using System.Globalization;

namespace ChaxAPI.Extension
{
    // Custom exception class for throwing application specific exceptions (e.g. for validation) 
    // that can be caught and handled within the application
    public class ChaxException : Exception
    {
        public ChaxException() : base() { }

        public ChaxException(string message) : base(message) { }

        public ChaxException(string message, params object[] args)
            : base(String.Format(CultureInfo.CurrentCulture, message, args))
        {
        }
    }
}
