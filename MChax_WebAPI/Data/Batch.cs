﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.DTO;
using ChaxAPI.Models;
using Dapper.Contrib.Extensions;

namespace ChaxAPI.Data
{
    public class Batch : IBatchService
    {
        public async Task<List<BATCH>> GetAllAsync()
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.GetAllAsync<BATCH>()).ToList();
            }
        }

        public async Task<BATCH> GetId(int id)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.GetAsync<BATCH>(id));
            }
        }

        public async Task<int> CreateAsync(BATCH banks)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.InsertAsync(banks));
            }
        }

        public async Task<bool> UpdateAsync(BATCH banks)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.UpdateAsync(banks));
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.DeleteAsync(new BATCH { ID = id}));
            }
        }
    }
}