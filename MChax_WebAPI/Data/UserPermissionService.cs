﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.DTO;
using ChaxAPI.Models;
using Dapper.Contrib.Extensions;

namespace ChaxAPI.Data
{
    public class UserPermissionService : IUserPermissionService
    {
        public async Task<List<USERPERMISSIONS>> GetAllAsync()
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.GetAllAsync<USERPERMISSIONS>()).ToList();
            }
        }

        public async Task<USERPERMISSIONS> GetId(int id)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.GetAsync<USERPERMISSIONS>(id));
            }
        }

        public async Task<int> CreateAsync(USERPERMISSIONS user)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.InsertAsync(user));
            }
        }

        public async Task<bool> UpdateAsync(USERPERMISSIONS user)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.UpdateAsync(user));
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.DeleteAsync(new USERPERMISSIONS { ID = id}));
            }
        }
    }
}