﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.DTO;
using ChaxAPI.Models;
using Dapper.Contrib.Extensions;

namespace ChaxAPI.Data
{
    public class PayeeService : IPayeeService
    {
        public async Task<List<PAYEE>> GetAllAsync()
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.GetAllAsync<PAYEE>()).ToList();
            }
        }

        public async Task<PAYEE> GetId(int id)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.GetAsync<PAYEE>(id));
            }
        }

        public async Task<int> CreateAsync(PAYEE payee)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.InsertAsync(payee));
            }
        }

        public async Task<bool> UpdateAsync(PAYEE payee)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.UpdateAsync(payee));
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.DeleteAsync(new PAYEE{ ID = id}));
            }
        }
    }
}