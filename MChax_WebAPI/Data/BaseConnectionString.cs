﻿using Microsoft.Extensions.Configuration;

namespace ChaxAPI.Data
{
    public class BaseConnectionString
    {
        public virtual string ConnectionString { get; }

        protected BaseConnectionString(IConfiguration configuration)
        {
            ConnectionString = new Connection(configuration).ConnectionString;
        }

//        protected BaseConnectionString(string datasource, string database, string userId, string password, int connectionTimeOut) =>
//            ConnectionString = new Connection
//            {
//                DbSourceRc = datasource,
//                Database = database,
//                UserId = userId,
//                Password = password,
//                ConnectionTimeOut = connectionTimeOut
//            }.ConnectionString;

//        public BaseConnectionString(string databaseName) => ConnectionString = new Connection { Database = databaseName }.ConnectionString;
//
//        public BaseConnectionString(int connectionTimeOut) => ConnectionString = new Connection { ConnectionTimeOut = connectionTimeOut }.ConnectionString;
    }
}