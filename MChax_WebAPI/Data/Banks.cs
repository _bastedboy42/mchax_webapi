﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using ChaxAPI.Component;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.DTO;
using ChaxAPI.Models;
using Dapper;
using Dapper.Contrib.Extensions;

namespace ChaxAPI.Data
{
    public class Banks: IBankService
    {
        public IEnumerable<BANK> GetAll()
        {
            using var con = new SqlConnection(Constants.ConnectionString);
            return con.GetAll<BANK>();
        }

        public BANK GetBankByRouting(string routingNumber)
        {
            string sql = "SELECT * FROM BANKS WHERE ROUTING=@ROUTING";
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return con.Query<BANK>(sql, new {ROUTING = routingNumber}).FirstOrDefault();
            }
        }

        public int Create(BANK bank)
        {
            using var con = new SqlConnection(Constants.ConnectionString);
            return (int) con.Insert(bank);
        }

        public bool Update(BANK bank)
        {
            using var con = new SqlConnection(Constants.ConnectionString);
            return con.Update(bank);
        }

        public bool Delete(int id)
        {
            using var con = new SqlConnection(Constants.ConnectionString);
            return con.Delete(new BANK {ID = id});
        }

        public async Task<IEnumerable<BANK>> GetAllAsync()
        {
            using var con = new SqlConnection(Constants.ConnectionString);
            return await con.GetAllAsync<BANK>();
        }

        public async Task<BANK> GetId(int id)
        {
            using var con = new SqlConnection(Constants.ConnectionString);
            return await con.GetAsync<BANK>(id);
        }

        public async Task<BANK> GetBankByRoutingAsync(string routingNumber)
        {
            string sql = "SELECT * FROM BANKS WHERE ROUTING=@ROUTING";
            using (var con = new SqlConnection(Constants.ConnectionString))
            {

                return (await con.QueryAsync<BANK>(sql, new {ROUTING = routingNumber})).FirstOrDefault();
            }
        }

        public async Task<int> CreateAsync(BANK bank)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
               return await con.InsertAsync(bank);
            }
        }

        public async Task<bool> UpdateAsync(BANK bank)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return await con.UpdateAsync(bank);
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return await con.DeleteAsync(new BANK{ ID = id});
            }
        }
    }
}

