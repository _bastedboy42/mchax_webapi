﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.DTO;
using ChaxAPI.Models;
using Dapper.Contrib.Extensions;

namespace ChaxAPI.Data
{
    public class TransactionService : ITransactionService
    {
        public async Task<List<TRANSACTION>> GetAllAsync()
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.GetAllAsync<TRANSACTION>()).ToList();
            }
        }

        public async Task<TRANSACTION> GetId(int id)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.GetAsync<TRANSACTION>(id));
            }
        }

        public async Task<int> CreateAsync(TRANSACTION transaction)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.InsertAsync(transaction));
            }
        }

        public async Task<bool> UpdateAsync(TRANSACTION transaction)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.UpdateAsync(transaction));
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.DeleteAsync(new TRANSACTION{ID = id}));
            }
        }
    }
}