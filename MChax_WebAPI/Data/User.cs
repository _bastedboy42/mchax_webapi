﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using ChaxAPI.Component;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.DTO;
using ChaxAPI.Extension;
using ChaxAPI.Models;
using Dapper;
using Dapper.Contrib.Extensions;

namespace ChaxAPI.Data
{
    public class User : IUserService
    {
        public USER GetByEmailAddress(string emailAddress)
        {
            string sql = "SELECT * FROM USERS WHERE EMAIL=@EMAIL";
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return con.Query<USER>(sql, new { EMAIL = emailAddress }).FirstOrDefault();
            }
        }

        public USER Create(USER user, string password)
        {
            if (string.IsNullOrWhiteSpace(password))
                throw new ChaxException("Password is required");

            var checkUserIfExist = GetByUserName(user.USERNAME);
            if (checkUserIfExist!=null)
            {
                throw new ChaxException("Username \"" + user.USERNAME + "\" is already taken");
            }

            var emailExist = GetByEmailAddress(user.EMAIL);
            if (emailExist!=null)
            {
                throw new ChaxException("Email address \"" + user.USERNAME + "\" is already exist");
            }


            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(password, out passwordHash, out passwordSalt);

            user.PASSWORDHASH = passwordHash;
            user.PASSWORDSALT = passwordSalt;

            Create(user);
            return user;
        }

        public USER Authenticate(string emailAddress, string password)
        {
            if (string.IsNullOrEmpty(emailAddress) || string.IsNullOrEmpty(password))
                return null;
            var user = GetByEmailAddress(emailAddress);

            // check if username exists
            if (user == null)
                return null;

            // check if password is correct
            if (!VerifyPasswordHash(password, user.PASSWORDHASH, user.PASSWORDSALT))
                return null;

            // authentication successful
            return user;
        }


        // private helper methods

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }


        #region Queries

        public IEnumerable<USER> GetAll()
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return con.GetAll<USER>();
            }
        }

        public async Task<IEnumerable<USER>> GetAllAsync()
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await  con.GetAllAsync<USER>()).ToList();
            }
        }

        public USER GetById(int id)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return con.Get<USER>(id);
            }
        }

        public USER GetByUserName(string username)
        {
            string sql = "SELECT * FROM USERS WHERE USERNAME=@USERNAME";
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return con.Query<USER>(sql, new { USERNAME = username }).FirstOrDefault();
            }
        }

       

        public void Create(USER user)
        {
            using (var con=new SqlConnection(Constants.ConnectionString))
            {
                con.Insert(user);
            }
        }

        public void Update(USER user)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                con.Update(user);
            }
        }

        public void Update(USER userParam, string password = null)
        {
            var user = GetById(userParam.USERID);

            if (user == null)
                throw new ChaxException("User not found");

            // update username if it has changed
            if (!string.IsNullOrWhiteSpace(userParam.USERNAME) && userParam.USERNAME != user.USERNAME)
            {
                // throw error if the new username is already taken
//                if (_context.Users.Any(x => x.Username == userParam.Username))
                    var username=GetByUserName(userParam.USERNAME);
                    if (username!=null)
                    {
                          throw new ChaxException("Username " + userParam.USERNAME + " is already taken");
                    }
                  

                user.USERNAME = userParam.USERNAME;
            }

            // update user properties if provided
            if (!string.IsNullOrWhiteSpace(userParam.FIRSTNAME))
                user.FIRSTNAME = userParam.FIRSTNAME;

            if (!string.IsNullOrWhiteSpace(userParam.LASTNAME))
                user.LASTNAME = userParam.LASTNAME;

            // update password if provided
            if (!string.IsNullOrWhiteSpace(password))
            {
                byte[] passwordHash, passwordSalt;
                CreatePasswordHash(password, out passwordHash, out passwordSalt);

                user.PASSWORDHASH = passwordHash;
                user.PASSWORDSALT = passwordSalt;
            }

            Update(user);
        }

        public void Delete(int id)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                con.Delete( new USER{ USERID = id});
            }
        }

        #endregion
    }
}
