﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using ChaxAPI.Component.Interfaces;
using ChaxAPI.DTO;
using ChaxAPI.Models;
using Dapper.Contrib.Extensions;

namespace ChaxAPI.Data
{
    public class SettingService : ISettingService
    {
        public async Task<List<SETTING>> GetAllAsync()
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.GetAllAsync<SETTING>()).ToList();
            }
        }

        public async Task<SETTING> GetId(int id)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.GetAsync<SETTING>(id));
            }
        }

        public async Task<int> CreateAsync(SETTING setting)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.InsertAsync(setting));
            }
        }

        public async Task<bool> UpdateAsync(SETTING setting)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.UpdateAsync(setting));
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            using (var con = new SqlConnection(Constants.ConnectionString))
            {
                return (await con.DeleteAsync(new SETTING { ID = id}));
            }
        }
    }
}