﻿using Microsoft.Extensions.Configuration;

namespace ChaxAPI.Data
{
    public class Connection
    {
       
        public readonly IConfiguration Configuration;
        #region Private Members
//        string _source = ConfigurationManager.AppSettings.Get("Data Source");
//        string _dbName = ConfigurationManager.AppSettings.Get("Database");
//        string _userId = ConfigurationManager.AppSettings.Get("UserId");
//        string _password = WinCrypto.Decrypt(ConfigurationManager.AppSettings.Get("Password"));
        #endregion
        #region Constructor

        

        public Connection(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        #endregion
        #region Properties

        /// <summary>
        /// IP Address or Computer Name
        /// </summary>
//        public string DbSourceRc
//        {
//            get => _source;
//            set => _source = value;
//        }
//
//
//        /// <summary>
//        /// Database Name
//        /// </summary>
//        public string Database
//        {
//            get => _dbName;
//            set => _dbName = value;
//        }
//
//        /// <summary>
//        /// Database User
//        /// </summary>
//        public string UserId
//        {
//            get => _userId;
//            set => _userId = value;
//        }
//
//        /// <summary>
//        /// Database Password
//        /// </summary>
//        public string Password
//        {
//            get => _password;
//            set => _password = value;
//        }

        /// <summary>
        /// Connection Timeout
        /// </summary>
        public int ConnectionTimeOut { get; set; } = 25;
        public string ConnectionString => Configuration.GetConnectionString("WebApiDatabase");

        #endregion
    }
}