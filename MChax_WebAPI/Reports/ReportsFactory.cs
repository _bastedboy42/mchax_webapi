﻿using System;
using System.Collections.Generic;

namespace ChaxAPI.Reports
{
    public static class ReportsFactory
    {
        public static Dictionary<string, Func<DevExpress.XtraReports.UI.XtraReport>> Reports = new Dictionary<string, Func<DevExpress.XtraReports.UI.XtraReport>>()
        {
            ["CheckLayout"] = () => new CheckLayout(),
            ["CheckLayoutBatchPrinting"] = () => new CheckLayoutBatchPrinting(),
        };
    }
}